# Vdiff

Visual diff tool for image. Written in Rust. Outputs PNG files similar to ImageMagick, but 10-30x faster.

## Supported Image Formats:
* bmp
* gif
* hdr
* ico
* jpeg
* png
* ppm
* tga
* tiff
* webp

## Usage
```
Provides a variety of ways to compare images, producing human or machine-readable output

USAGE:
    vdiff [FLAGS] <INPUT1> <INPUT2> [ARGS]

FLAGS:
    -f, --force       Force overwriting when the destination file exists
    -h, --help        Prints help information
    -d, --maxdiff     Sets a threshold (0-255) above which 2 pixels will be considered different
        --subimage    Search for the smaller image contained within the larger one
    -V, --version     Prints version information
    -v                Sets the level of verbosity (none through vvv)

ARGS:
    <INPUT1>    Set the first image to compare
    <INPUT2>    Set the second image to compare
    <OUT>       Set basename of the output file(s). Some comands may produce multiple output files. The default is vdiff-%1-%2.<ext> where the extension matches the INPUT1.
```

## More information
The [Development Diary](devlog/GettingStarted.md) contains benchmarks, example outputs, and a journal of the development of this repo. 
