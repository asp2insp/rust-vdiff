## How fast is fast?

Let's look at where we stack up against some of the competition (of course disregarding the fact that they have way more features and options) Let's check out this matrix of test images:

|                   | Medium                    | Large                                      |
|-------------------|---------------------------|--------------------------------------------|
| No Differences    | penguin_curl/penguin_curl | macaroni_penguin/macaroni_penguin          |
| Small Differences | penguin_curl/penguin_bow  | macaroni_penguin_modified/macaroni_penguin |
| All Different     | penguin_curl/penguin_santa| macaroni_penguin/adelie_penguin            |

### Medium Images (1280x1280) by PeterM66 (Pixabay)
- penguin_curl
- penguin_bow
- penguin_santa

### Large Images (5616×3744)
- macaroni_penguin by Liam Quinn  (Flikr)
- adelie_penguin by By Jason Auch (Flikr)

Here are the results. All values are an average of the last 3 runs out of a set of 4 runs, `vdiff` used the `-f` option, `compose` used no options and `blink-diff` uses the `--no-composition` option to match the same format that the other tools output by default.

| tool      | Med_No_Diff  | Med_Small_Diff   | Med_All_diff |
|-----------|--------------|------------------|--------------|
| vdiff     | 0.080s       | 0.076s           | 0.075s       |
| compare   | 0.469s       | 0.456s           | 0.458s       |
| blink-diff| 0.769s       | 0.892s           | 2.096s       |

| tool      | Lrg_No_Diff  | Lrg_Small_Diff   | Lrg_All_diff |
|-----------|--------------|------------------|--------------|
| vdiff     | 02.676s      | 02.806s          | 01.772s      |
| compare   | 13.432s      | 13.514s          | 13.464s      |
| blink-diff| 08.692s      | 08.641s          | 26.661s      |

Here are the resulting images for the tests:

[TODO images here]

It's really interesting that blink-diff is so much more expensive for images that are mostly different. It's also pretty cool that we're already faster than the competition. But there's one thing that bugs me:

[single_threaded.png]

Only one thread. One of the goals of Rust is fearless concurrency, surely we can do much better than that!

Let's start by loading our images in separate threads. Note the double `.expect()` unwrapping. The first unwraps the `Result` from the thread join, the second unwraps the "inner" `Result` which represents the loading of the image.

```
use std::thread;

// ...
let handle_1 = thread::spawn(|| {
    image::open(&input_1)
});
let handle_2 = thread::spawn(|| {
    image::open(&input_2)
});

let img_1 = handle_1.join()
    .expect("Threading Error")
    .expect(format!("Could not open image: {}", input_1.to_string_lossy()).as_str());
let img_2 = handle_2.join()
    .expect("Threading Error")
    .expect(format!("Could not open image: {}", input_2.to_string_lossy()).as_str());
```

Uh-oh! The borrow checker gives us an error:

```
error: `matches` does not live long enough
  --> src/main.rs:33:29
   |
33 |     let input_2 = Path::new(matches.value_of("INPUT2").unwrap());
   |                             ^^^^^^^ does not live long enough
...
87 | }
   | - borrowed value only lives until here
   |
   = note: borrowed value must be valid for the static lifetime...
```

What's going on here? Well let's take a look at the documentation for [Path] `Path::new` states that

> Directly wrap a string slice as a Path slice.
> This is a cost-free conversion.

Well that means that we must be borrowing the input string, and when we borrow the path in another thread, the compiler can no longer guarantee the lifetime by looking at the scope (never mind that we call `join()`). So, we need to use `to_owned()` to properly send a safe copy of the path over.

```
let input_1_copy = input_1.to_owned();
let handle_1 = thread::spawn(|| {
    image::open(input_1_copy)
});
```

Now, when we run our resulting binary we see the following!
[threaded_load.png]

Are we any faster? Let's try our benchmarks again:

| tool        | Lrg_No_Diff  | Lrg_Small_Diff   | Lrg_All_diff |
|-------------|--------------|------------------|--------------|
| vdiff_single| 02.676s      | 02.806s          | 01.772s      |
| vdiff_multi | 01.006s      | 01.053s          | 00.980s      |

Woohoo! We're almost twice as fast across the board. But we've only parallelized the first part of the work -- loading the images. What does our CPU profile look like? Let's fire up Instruments and watch the CPU vs time. I followed this [great tutorial](http://carol-nichols.com/2015/12/09/rust-profiling-on-osx-cpu-time/) for how to set up my cargo config for profiling. We can see a profile that looks something like this:

![CPU profile of a high and then a low area](cpu_prof_stepdown.png)

Which is what we expected. We use 2 full cores during the image loads, but only one core during the processing of the pixels. The natural solution is to segment the image into subregions and process each region on a different thread. We're going to use [rayon](https://github.com/nikomatsakis/rayon) which is a crate that allows us to easily express parallel iteration. We're going to pre-allocate our output buffer and then break it into mutable chunks that we can iterate over. Here's a first pass:

```
let width = dim_1.0 as usize;
let height = dim_1.1 as usize;
let mut out_buffer: Vec<u8> = vec![0u8; width * height * 4];
out_buffer.par_chunks_mut(4)
    .enumerate()
    .for_each(|(index, px)| {
        let x = (index % width) as u32;
        let y = (index / width) as u32;
        let pixel_1 = img_1.get_pixel(x, y).to_rgba();
        let pixel_2 = img_2.get_pixel(x, y).to_rgba();

        if meets_diff_threshold(pixel_1, pixel_2, max_diff) {
            // Return a low-alpha copy of the pixel
            px.clone_from_slice(&[pixel_1[0], pixel_1[1], pixel_1[2], 64u8]);
        } else {
            px.clone_from_slice(&[255u8, 0u8, 0u8, 255u8]);
        }
    });
image::save_buffer(&output_path, &outbuffer, dim_1.0, dim_1.1, image::RGBA(8))
    .expect(format!("Error while saving to {}", output).as_str());
```

This brings our time down to ~.910 seconds on average for Lrg_Small_Diff, and when we look at our CPU profile:

![CPU profile of 8 threads](eight_threads.png)

We see that the bulk of our time is spent in the load, using 2 CPUs, then there's a sharp burst of usage which takes all 8 cores available, followed by a long time spent writing our output to disk on a single core. Since we are now IO bound, I think it's time to add some more interesting functionality to our algorithm.

[Next up: Subimage Detection](Subimage.md)
