## Choosing a Name

Hardest part of the project really, pdiff, imgdiff, all taken. Let's go with vdiff for now, easy enough to change later.

## Libraries!
Off to crates.io to fetch some helpers

```
image = "0.10.3" # Load images
clap = {version = "2", features = ["yaml"]} # Parse command line flags (with YAML feature)
```

Now we add some code and build for the very first time

```
fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    let input_1 = matches.value_of("INPUT1").unwrap();
    let input_2 = matches.value_of("INPUT2").unwrap();
    let output = matches.value_of("OUT").unwrap_or(format!("vdiff-{}-{}.png"));
    let v_level = match matches.occurrences_of("v");

    let img_1 = image::open(&Path::new(input_1))
        .expect(format!("Could not open image: {}", input_1));
    let img_2 = image::open(&Path::new(input_2))
        .expect(format!("Could not open image: {}", input_2));

    println!("Chosen Image dimensions {:?}", img_1.dimensions());
    println!("Chosen Image dimensions {:?}", img_2.dimensions());
    let ref mut fout = File::create(&Path::new(output))
        .expect(format!("Could not open {} for writing", output));

    let _ = img.save(fout, image::PNG)
        .expect(format!("Error while saving to {}", output));
}
```

So we're going to open two image files, parse them, spit out some info, then write to a new file and exit.

```
$ cargo run -- --help
    Finished debug [unoptimized + debuginfo] target(s) in 0.0 secs
     Running `target/debug/vdiff --help`
vdiff 0.0.1
Provides a variety of ways to compare images, producing human or machine-readable output

USAGE:
    vdiff [FLAGS] <INPUT1> <INPUT2> [ARGS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v               Sets the level of verbosity (none through vvv)

ARGS:
    <INPUT1>     Set the first image to compare
    <INPUT2>     Set the second image to compare
    <OUTNAME>    Set basename of the output file(s). Some comands may produce multiple output
                 files. The default is vdiff-%1-%2.<ext> where the extension matches the INPUT1.
```

Note the extra `--` that separates the arguments to the program from arguments to cargo.
`$ cargo run -- testdata/penguin_curl.png testdata/penguin_bow.png testdata/out.png`
Heya! It works!

> TODO:
> 1) Add some handling to warn before clobbering
> 2) Fix basename handling for default name generation
> 3) Clean up messy code

[Next Up: Functionality and Cleanliness](FunctionalityAndCleanliness.md)
