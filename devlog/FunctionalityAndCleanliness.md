## Cleanliness is next to godliness
[rustfmt](https://github.com/rust-lang-nursery/rustfmt) is an awesome tool that you can run on your code to fix your indentation woes forever.

```
$ cargo install rustfmt
$ rustfmt src/*.rs
```
Oh so pretty!

[Clippy](https://github.com/Manishearth/rust-clippy) is a set of lints that help you clean up your code. You can enable it by adding an optional `dependency` and then enabling clippy with a feature flag

```
[dependencies]
clippy = {version = "*", optional = true}

[features]
default = []
```

And the following sets the flag at the top of your `main.rs`
```
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
```

Now, whenever you run `cargo build --features clippy` you will get additional lints and warnings for your code.

> Note that clippy requires a nightly compiler. If you're not on nightly, you'll
> need to run the following:
> `$ rustup toolchain install nightly` or `$ rustup update` if you already have nightly installed
> `$ rustup override set nightly`
> This installs the latest nightly and sets it as the default for the current directory

Clippy fixed the following errors:

```
warning: `ref` on an entire `let` pattern is discouraged, take a reference with `&` instead, #[warn(toplevel_ref_arg)] on by default
  --> src/main.rs:39:9
   |
39 |     let ref mut fout = File::create(&Path::new(output))
```

Code is now:
```
let mut fout = File::create(&Path::new(output))
    .expect(format!("Could not open {} for writing", output).as_str());
img_1.save(&mut fout, image::PNG)
    .expect(format!("Error while saving to {}", output).as_str());
```

## Functionality

Whew! It's time to make our program actually do something. We're going to start by asserting that our images are the same size (we'll relax this restriction later):

```
$ cargo run -- testdata/penguin_curl_right.png testdata/penguin_curl.png
   Compiling vdiff v0.1.0 (file:///Users/jcg/Source/vdiff)
    Finished debug [unoptimized + debuginfo] target(s) in 1.60 secs
     Running `target/debug/vdiff testdata/penguin_curl_right.png testdata/penguin_curl.png`
thread 'main' panicked at 'Error: the given images must be the same size, (642, 1268) vs (1280, 1280)', src/main.rs:37
note: Run with `RUST_BACKTRACE=1` for a backtrace.
```

Now, we're going to iterate pixel by pixel. If the pixels are identical, we will output a mostly white pixel. If they are significantly different (by some threshold) we will output a red pixel. This mimicks what ImageMagick's `compare` executable does.

We're going to add a new flag to optionally set the threshold with a very scientific default of 10:

```
- maxdiff:
    short: d
    help: Sets a threshold (0-255) above which 2 pixels will be considered different
```

Then read that flag and compute our difference image
```
let max_diff: usize = matches.value_of("maxdiff").unwrap_or(10u8);
// Compare pixel-by-pixel and build the output image
let outimg = ImageBuffer::from_fn(dim_1.0, dim_1.1, |x, y| {
    let pixel_1 = img_1.get_pixel(x, y).to_rgba();
    let pixel_2 = img_2.get_pixel(x, y).to_rgba();

    let is_same = {
        pixel_1[0].overflowing_sub(pixel_2[0]).0 < max_diff &&
        pixel_1[1].overflowing_sub(pixel_2[1]).0 < max_diff &&
        pixel_1[2].overflowing_sub(pixel_2[2]).0 < max_diff &&
        pixel_1[3].overflowing_sub(pixel_2[3]).0 < max_diff
    };
    if is_same {
        // Return a low-alpha copy of the pixel
        image::Rgba([pixel_1[0], pixel_1[1], pixel_1[2], 64u8])
    } else {
        image::Rgba([255u8, 0u8, 0u8, 255u8])
    }
});
outimg.save(&Path::new(output))
    .expect(format!("Error while saving to {}", output).as_str());
```
So... does it work?? It does!
![difference](f_a_c_diff_1.png) ![input 1](../testdata/penguin_bow.png) ![input 2](../testdata/penguin_curl.png)

How's the performance? (Remember to run `cargo build --release` before benchmarking):
`$ time ./target/release/vdiff testdata/penguin_curl.png testdata/penguin_bow.png`
Average over 3 runs (with `compare` as a comparison):

| tool    | Wall  | User   |
|---------|-------|--------|
| vdiff   |0.078s | 0.066s |
| compare |0.445s | 0.418s |

Output comparison:
![Output of Vdiff](f_a_c_diff_1.png)
![Output of Compare](f_a_c_cmp_1.png)

I started this project because `compare` was slow, but I didn't expect to be so much faster right away.

## Oof, what happened to cleanliness?

Look at all that ugly code. There's duplicated code, everything's embedded in the main... Let's clean things up:

```
fn abs_diff_lt(a: u8, b: u8, max_diff: u8) -> bool {
    a.overflowing_sub(b).0 < max_diff
}

fn meets_diff_threshold(pixel_a: Rgba<u8>, pixel_b: Rgba<u8>, max_diff: u8) -> bool {
    {
        abs_diff_lt(pixel_a[0], pixel_b[0], max_diff) &&
        abs_diff_lt(pixel_a[1], pixel_b[1], max_diff) &&
        abs_diff_lt(pixel_a[2], pixel_b[2], max_diff) &&
        abs_diff_lt(pixel_a[3], pixel_b[3], max_diff)
    }
}
// ... and back in the main loop:
  // Compare pixel-by-pixel and build the output image
  let outimg = ImageBuffer::from_fn(dim_1.0, dim_1.1, |x, y| {
      let pixel_1 = img_1.get_pixel(x, y).to_rgba();
      let pixel_2 = img_2.get_pixel(x, y).to_rgba();

      if meets_diff_threshold(pixel_1, pixel_2, max_diff) {
          // Return a low-alpha copy of the pixel
          Rgba([pixel_1[0], pixel_1[1], pixel_1[2], 64u8])
      } else {
          Rgba([255u8, 0u8, 0u8, 255u8])
      }
  });
```
much better already! If you're worried about the overhead of these extra function calls, fear not, tiny functions like this will almost certainly be inlined by compiler. If you're worried, you can always check yourself:

```
$ cargo rustc --release -- --emit=llvm-ir
$ cat target/release/vdiff.ll
```

Great! We've fixed up our items from the [last post](GettingStarted.md), and implemented some real functionality.

> TODO So what's next?
> 1) Multithreading. We're already fast, but only running on a single core
> 2) Add perceptual diffs or leveshtein-style diffing

[Next up: Moar Threads](MoarThreads.md)
