# Finding Subimages

Now that we have pixel-for-pixel comparisons done, let's try a slightly harder problem: what if our input images are not the same size? A naive approach would be to take the smaller of the two images and offset it into the larger image, then move it one pixel at a time and find the one with the lowest score.

I've factored out the diff-finding image into a function:
```
fn get_diff_score(needle: &Box<DynamicImage>,
                  haystack: &Box<DynamicImage>,
                  x_offset: u32,
                  y_offset: u32,
                  width: usize,
                  height: usize,
                  threshold: u8)
                  -> Diff {
    // Compare pixel-by-pixel and build the output image
    let mut out_buffer: Vec<u8> = vec![0u8; width * height * 4];

    let num_differences = out_buffer.par_chunks_mut(4)
        .enumerate()
        .map(|(out_index, out_px)| {
            let x = (out_index % width) as u32;
            let y = (out_index / width) as u32;
            let pixel_1 = needle.get_pixel(x, y).to_rgba();
            let pixel_2 = haystack.get_pixel(x + x_offset, y + y_offset).to_rgba();

            if meets_diff_threshold(pixel_1, pixel_2, threshold) {
                // Return a low-alpha copy of the pixel
                out_px.clone_from_slice(&[pixel_1[0], pixel_1[1], pixel_1[2], 64u8]);
                0
            } else {
                out_px.clone_from_slice(&[255u8, 0u8, 0u8, 255u8]);
                1
            }
        })
        .sum();
    Diff {
        total_pixels: width * height,
        different_pixels: num_differences,
        out_buffer: out_buffer,
        x_offset: x_offset,
        y_offset: y_offset,
        width: width,
        height: height,
    }
}
```

and now we can iterate over all the possible sub-image locations of the smaller image:

```
// img_1 will be the smaller of the two
let needle = if dim_1 > dim_2 { &img_2 } else { &img_1 };
let haystack = if dim_1 > dim_2 { &img_1 } else { &img_2 };

let width = needle.width();
let height = needle.height();
let search_space: Vec<(u32, u32)> = (0..haystack.width() - width)
    .flat_map(|x| (0..(haystack.height() - height)).map(move |y| (x, y)))
    .collect();

search_space.par_iter()
    .map(|&(x, y)| {
        get_diff_score(needle,
                       haystack,
                       x,
                       y,
                       width as usize,
                       height as usize,
                       max_diff)
    })
    .min()
```
And does it work? yep!
![ouput image](subimage.png) from ![Input 1 image](../testdata/penguin_curl_right.png) and ![Input 2 image](../testdata/penguin_curl.png)

How does the runtime compare? The times below are the average of the final 3 out of 4 rounds.

| tool      | Med_Right_Half  | Med_Left_Half |
|-----------|-----------------|---------------|
| vdiff     | 033.994s        | 02.719s       |
| compare   | 946.095s *      | 78.189s       |
| blink-diff| 054.704s        | 00.523s       |

> * only run once because it took forever, but I made sure the filesystem cache was warm

But really we're cheating here since we're not printing the full image, only the matching overlay Let's fix that (even though blink-diff does this too):

```
let needle_x_bound = needle.width() + x_offset;
let needle_y_bound = needle.height() + y_offset;
let num_differences = out_buffer.par_chunks_mut(4)
    .enumerate()
    .map(|(out_index, out_px)| {
        let x = (out_index % width) as u32;
        let y = (out_index / width) as u32;
        let pixel_1 = haystack.get_pixel(x, y).to_rgba();
        if x < x_offset || y < y_offset || x >= needle_x_bound || y >= needle_y_bound {
            // If we're outside the bounds of the search region, ignore needle, darken output
            out_px.clone_from_slice(&[pixel_1[0]/2, pixel_1[1]/2, pixel_1[2]/2, 128u8]);
            0
        } else {
            let pixel_2 = needle.get_pixel(x - x_offset, y - y_offset).to_rgba();
            if meets_diff_threshold(pixel_1, pixel_2, threshold) {
                // Return a low-alpha copy of the pixel
                out_px.clone_from_slice(&[pixel_1[0], pixel_1[1], pixel_1[2], 64u8]);
                0
            } else {
                out_px.clone_from_slice(&[255u8, 0u8, 0u8, 255u8]);
                1
            }
        }
    })
    .sum();
```

We add an extra bounds check and iterate over the larger image rather than the smaller one.

We're now slower than our competition though, and part of that is because we always do ALL of the work, and we have no way of canceling work early. We also OOM whenever the search space is large.

Let's add a quick optimization to allow us to remove some of the retained memory as we go. After we've calculated the diff score for a given offset pair, we can check against the current global best, and if we don't beat it, we can throw away the image buffer immediately.

```
loop {
    let current_lowest = score_to_beat.load(Ordering::SeqCst);
    if current_lowest < num_differences {
        // If we're already not the lowest, then we won't need our pixels.
        out_buffer.clear();
        break;
    }
    if score_to_beat.compare_and_swap(current_lowest, num_differences, Ordering::SeqCst) == num_differences {
        break;
    }
}
```

Great! Let's optimize a little further down this path by upgrading our search space generation. Right now we naively iterate over all pairs of `(x_offset, y_offset)` in order. We should be able to do much better than that. What if we started by matching significant features or clusters in the image and that would give a basis for where to begin looking?

> TODO explore feature matching or clustering
