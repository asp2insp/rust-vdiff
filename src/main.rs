#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]

extern crate image;
#[macro_use]
extern crate clap;
extern crate rayon;

use std::path::Path;
use std::ffi::OsStr;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::SeqCst;
use std::cmp::Ordering;

use image::{GenericImage, DynamicImage, Pixel, Rgba};
use clap::App;
use rayon::prelude::*;
use rayon::par_iter::reduce::{MinOp, ReduceOp};

fn abs_diff_lt(a: u8, b: u8, max_diff: u8) -> bool {
    a.overflowing_sub(b).0 < max_diff
}

fn meets_diff_threshold(pixel_a: Rgba<u8>, pixel_b: Rgba<u8>, max_diff: u8) -> bool {
    {
        abs_diff_lt(pixel_a[0], pixel_b[0], max_diff) &&
        abs_diff_lt(pixel_a[1], pixel_b[1], max_diff) &&
        abs_diff_lt(pixel_a[2], pixel_b[2], max_diff) &&
        abs_diff_lt(pixel_a[3], pixel_b[3], max_diff)
    }
}

struct Diff {
    total_pixels: usize,
    different_pixels: usize,
    out_buffer: Vec<u8>,
    x_offset: u32,
    y_offset: u32,
    width: usize,
    height: usize,
}

impl ReduceOp<Diff> for MinOp {
    fn start_value(&self) -> Diff {
        Diff {
            total_pixels: 1,
            different_pixels: usize::max_value(),
            out_buffer: vec![],
            x_offset: 0,
            y_offset: 0,
            width: 0,
            height: 0,
        }
    }

    fn reduce(&self, value1: Diff, value2: Diff) -> Diff {
        if value1.different_pixels <= value2.different_pixels {
            value1
        } else {
            value2
        }
    }
}

fn get_diff_score(needle: &Box<DynamicImage>,
                  haystack: &Box<DynamicImage>,
                  x_offset: u32,
                  y_offset: u32,
                  width: usize,
                  height: usize,
                  threshold: u8,
                  score_to_beat: &AtomicUsize)
                  -> Diff {
    // Compare pixel-by-pixel and build the output image
    let mut out_buffer: Vec<u8> = vec![0u8; width * height * 4];
    let needle_x_bound = needle.width() + x_offset;
    let needle_y_bound = needle.height() + y_offset;
    let num_differences = out_buffer.par_chunks_mut(4)
        .enumerate()
        .map(|(out_index, out_px)| {
            let x = (out_index % width) as u32;
            let y = (out_index / width) as u32;
            let pixel_1 = haystack.get_pixel(x, y).to_rgba();
            if x < x_offset || y < y_offset || x >= needle_x_bound || y >= needle_y_bound {
                // If we're outside the bounds of the search region, ignore needle, darken output
                out_px.clone_from_slice(&[pixel_1[0]/2, pixel_1[1]/2, pixel_1[2]/2, 128u8]);
                0
            } else {
                let pixel_2 = needle.get_pixel(x - x_offset, y - y_offset).to_rgba();
                if meets_diff_threshold(pixel_1, pixel_2, threshold) {
                    // Return a low-alpha copy of the pixel
                    out_px.clone_from_slice(&[pixel_1[0], pixel_1[1], pixel_1[2], 64u8]);
                    0
                } else {
                    out_px.clone_from_slice(&[255u8, 0u8, 0u8, 255u8]);
                    1
                }
            }
        })
        .sum();

    loop {
        let current_lowest = score_to_beat.load(SeqCst);
        if current_lowest < num_differences {
            // If we're already not the lowest, then we won't need our pixels.
            out_buffer.clear();
            break;
        }
        if score_to_beat.compare_and_swap(current_lowest, num_differences, SeqCst) == num_differences {
            break;
        }
    }
    Diff {
        total_pixels: width * height,
        different_pixels: num_differences,
        out_buffer: out_buffer,
        x_offset: x_offset,
        y_offset: y_offset,
        width: width,
        height: height,
    }
}

fn compute_search_space(needle: &Box<DynamicImage>, haystack: &Box<DynamicImage>)
        -> Vec<(u32, u32)> {
    let mut search_space: Vec<(u32, u32)> = (0..haystack.width() - needle.width()).chain(0..1)
        .flat_map(|x| {
            (0..(haystack.height() - needle.height()))
                .chain(0..1)
                .map(move |y| (x, y))
        }).collect();
    let every_nth = 10;
    search_space.sort_by(|a, b| {
        if a.0 % every_nth == 0 || a.1 % every_nth == 0 {
            Ordering::Less
        } else if b.0 % every_nth == 0 || b.1 % every_nth == 0 {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    });
    search_space
}

fn main() {
    let yaml = load_yaml!("cli.yaml");
    let matches = App::from_yaml(yaml).get_matches();

    let input_1 = Path::new(matches.value_of("INPUT1").unwrap());
    let input_2 = Path::new(matches.value_of("INPUT2").unwrap());

    let default_name = format!("vdiff-{}-{}.png",
                               input_1.file_stem().unwrap_or(OsStr::new("one")).to_string_lossy(),
                               input_2.file_stem().unwrap_or(OsStr::new("two")).to_string_lossy());
    let output = matches.value_of("OUT")
        .unwrap_or(default_name.as_str());
    let v_level = matches.occurrences_of("verbose");

    let (img_1, img_2) =
        rayon::join(|| {
                        Box::new(image::open(input_1.to_owned())
                            .expect(format!("Could not open image: {}", input_1.to_string_lossy())
                                .as_str()))
                    },
                    || {
                        Box::new(image::open(input_2.to_owned())
                            .expect(format!("Could not open image: {}", input_2.to_string_lossy())
                                .as_str()))
                    });

    let dim_1 = img_1.dimensions();
    let dim_2 = img_2.dimensions();
    let find_subimage = matches.is_present("subimage");
    if dim_1 != dim_2 && !find_subimage {
        panic!("Error: the given images must be the same size, {:?} vs {:?}, {}",
               dim_1,
               dim_2,
               "pass --subimage to search for one image contained by another");
    }

    let max_diff = value_t!(matches, "maxdiff", u8).unwrap_or(10u8);

    // img_1 will be the smaller of the two
    let needle = if dim_1 > dim_2 { &img_2 } else { &img_1 };
    let haystack = if dim_1 > dim_2 { &img_1 } else { &img_2 };

    let output_width = haystack.width();
    let output_height = haystack.height();
    let min_score = AtomicUsize::new(usize::max_value());
    let search_space: Vec<(u32, u32)> = compute_search_space(&needle, &haystack);
    let diff = search_space.par_iter()
        .map(|&(x, y)| {
            get_diff_score(needle,
                           haystack,
                           x,
                           y,
                           output_width as usize,
                           output_height as usize,
                           max_diff,
                           &min_score)
        })
        .min();

    let output_path = Path::new(output);
    let force_output = matches.is_present("force");
    if output_path.exists() && !force_output {
        panic!("Error {} already exists on disk! Pass -f to overwrite",
               output)
    }
    if v_level > 0 {
        println!("Difference: {}/{}",
                 diff.different_pixels,
                 diff.total_pixels);
        if find_subimage {
            println!("Using ({},{}) as the top-left corner for subimage",
                     diff.x_offset,
                     diff.y_offset);
        }
    }
    if v_level > 1 {
        println!("Saving to {}", output);
    }

    image::save_buffer(&output_path,
                       &diff.out_buffer,
                       diff.width as u32,
                       diff.height as u32,
                       image::RGBA(8))
        .expect(format!("Error while saving to {}", output).as_str());
}
